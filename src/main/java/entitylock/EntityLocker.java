/*
 * Copyright (c) 2017, Dmitry Fazunenko and/or his affiliates.
 * All rights reserved.
 */
package entitylock;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

/**
 * Utility class that provides synchronization mechanism similar to row-level DB locking.
 * The class is supposed to be used by the components that are responsible for managing storage
 * and caching of different type of entities in the application.
 * EntityLocker itself does not deal with the entities, only with the IDs (primary keys) of the entities.
 *
 * Requirements:
 * 1. EntityLocker should support different types of entity IDs.
 * 2. EntityLocker's interface should allow the caller to specify which entity does it want
 *    to work with (using entity ID), and designate the boundaries of the code that
 *    should have exclusive access to the entity (called 'protected code').
 * 3. For any given entity, EntityLocker should guarantee that at most one thread executes
 *    protected code on that entity. If there's a concurrent request to lock the same entity,
 *    the other thread should wait until the entity becomes available.
 * 4. EntityLocker should allow concurrent execution of protected code on different entities.
 *
 * Implementation notes:
 * The current version implements deadlock proof mechanism providing exclusive access
 * to the protected code. Usage is similar to java.util.concurrent.Lock:
 *
 *
 *     EntityLocker locker = EntityLocker.getEntityLocker(resource);
 *     locker.lock(entityID);
 *     // protected code to work with entity which id is entityID
 *     locker.unlock(entityID);
 *
 * Protected code may invoke other methods keeping exclusive access rights:
 *
 *     final DataBase db = openDB();
 *
 *     void updateX(int rowN) throws DeadLockException, InterruptedException {
 *         EntityLocker locker = EntityLocker.getEntityLocker(db);
 *         locker.lock(new Integer(rowN));
 *         updateY("Y" + rowN);
 *         locker.unlock(new Integer(rowN));
 *     }
 *     void updateY(String rowID) throws DeadLockException, InterruptedException {
 *         EntityLocker locker = EntityLocker.getEntityLocker(db);
 *         locker.lock(rowID);
 *         // being invoked from updateX will have both rights to 'rowN' and 'rowID'
 *         locker.unlock(rowID);
 *     }
 *
 * An attempt to invoke the lock() method on the entity which would cause deadlock will be immediately
 * detected and result DeadLockException. The exception will contain entityID which should be
 * released to avoid deadlock.
 *
 * DeadLockException signals that one or several threads are waiting for some entity acquired by
 * the current thread. To move further the thread should release the entity and probably roll back
 * some modifications. (Handling deadlocks is out of the scope of EntityLocker mechanism).
 *
 * For the sake of convenience EntityLocker provides the tryLock(Object entityId) method which doesn't throw
 * DeadLockException. It returns null if the lock has been acquired or an entityId object to be released
 * to avoid deadlock:
 *         EntityLocker locker = EntityLocker.getEntityLocker(db);
 *         if (locker.tryLock(rowID) == null) {
 *             // protected code
 *             locker.unlock(rowID);
 *         }
 *
 */
public final class EntityLocker {

    private static final Map<Object, EntityLocker> resourceLockers = Collections.synchronizedMap(new WeakHashMap<>());

    private EntityLocker() {
    }

    /**
     * Returns the instance of EntityLocker associated with the given resource.
     *
     * @param resource  not null.
     * @return Never returns null.
     */
    public static synchronized EntityLocker getEntityLocker(Object resource) {
        if (resource == null) {
            throw new IllegalArgumentException("null resource is not allowed");
        }
        EntityLocker locker = resourceLockers.get(resource);
        if (locker == null) {
            locker = new EntityLocker();
            resourceLockers.put(resource, locker);
        }
        return locker;
    }


    // deque of IDs successfully locked by the thread
    private final ThreadLocal<Deque> threadLockedIds = new ThreadLocal<>();

    // amount of global locks acquired by the thread
    private final ThreadLocal<Integer> threadGlobalLocks = ThreadLocal.withInitial(() -> 0);

    // map: entityId -> entityLock, supposed to be clean by GC when entities are not used.
    private final Map<Object, EntityLock> id2lock = new WeakHashMap<>();

    private final Semaphore GLOBAL_LOCK = new Semaphore(Integer.MAX_VALUE);

    // guarantee: equal IDs have the same lock object
    private synchronized EntityLock getLockForID(Object id) {
        if (id == null) {
            throw new IllegalArgumentException("Null entity ID not supported");
        }
        EntityLock lock = id2lock.get(id);
        if (lock == null) {
            lock = new EntityLock();
            id2lock.put(id, lock);
        }
        return lock;
    }


    /**
     * Acquires the lock for the given entityId.
     * The method:
     * - returns null immediately if the entity is free
     * - throws DeadLockException immediately if locking would cause the deadlock
     * - waits until the entity is free and returns null
     * - throws InterruptedException if waiting was interrupted
     *
     * @param entityId id of entity to get exclusive access to
     * @throws DeadLockException if locking would cause the deadlock
     * @throws InterruptedException
     */
    public void lock(Object entityId) throws DeadLockException, InterruptedException {
        getLockForID(entityId).lock(entityId);
    }

    /**
     * Tries to acquire lock without deadlock.
     * Returns null in case of success or an entityId of entity to release to avoid deadlock.
     * This method just a wrapper to lock(Object entityId), so it will wait for entity is free
     * if waiting will not cause the deadlock.
     *
     * @param entityId id of entity to get exclusive access to
     * @return null if successfully locked or entity ID to release to avoid deadlock
     * @throws InterruptedException
     */
    public Object tryLock(Object entityId) throws InterruptedException {
        try {
            lock(entityId);
            return null;
        } catch (DeadLockException e) {
            return e.entityID;
        }
    }


    /**
     * Releases the lock if owned by the current thread.
     * Attempt to release not owned lock will cause IllegalStateException to be thrown.
     * @param entityId
     * @throws IllegalStateException if entityId is not owned by the current thread
     */
    public void unlock(Object entityId) {
        EntityLock lock = id2lock.get(entityId);
        if (lock == null) {
            throw new IllegalStateException("Lock hasn't been acquired for " + entityId);
        }
        lock.unlock(entityId);
    }

    /**
     * Releases all locks owned by the Thread
     */
    public void unlockAll() {
        Deque deque = lockedByThisThread();
        LinkedList list = new LinkedList();
        list.addAll(deque);
        for (Iterator it = list.descendingIterator(); it.hasNext(); ) {
            unlock(it.next());
        }
    }

    private Deque lockedByThisThread() {
        Deque deque = threadLockedIds.get();
        if (deque == null) {
            deque = new LinkedList<>();
            threadLockedIds.set(deque);
        }
        return deque;
    }

    // thread -> object to wait or null.
    private final Map<Thread, Object> thread2waiting = new ConcurrentHashMap<>();

    // entity_id -> thread owning the entity ID
    private final Map<Object, Thread> id2thread = new ConcurrentHashMap<>();


    // check if in attempt to wait for entityId owned by otherThread will lead to dead lock
    // if no deadlock expected the current thread is declared a waiter of entityId
    private synchronized void checkDeadLock(Thread otherThread, Object entityId) throws DeadLockException {
        Deque locked = lockedByThisThread();
        if (!locked.isEmpty()) {
            Object stopper = thread2waiting.get(otherThread);
            // resource waiting by the thread owning the lock we try to acquire
            for (;;) {
                if (stopper == null) {
                    break;
                } else if (locked.contains(stopper)) {
                    throw new DeadLockException(stopper);
                }
                Thread t2 = id2thread.get(stopper); // Thread owning stopper
                if (t2 == null) {
                    break;
                }
                stopper = thread2waiting.get(t2);
            }
        }
        thread2waiting.put(Thread.currentThread(), entityId);
        return;
    }

    // package private for the sake of testability
    class EntityLock {
        private Thread owner;
        // final Object entityId - we don't store entityId in the EntityLock class
        // to allow gc collect entries in the WeakHashMap

        EntityLock(){
        }

        private synchronized void lock(Object entityId) throws DeadLockException, InterruptedException {
            Thread currentThread = Thread.currentThread();
            if (owner == null) {
                locked(entityId);
                return;
            } else if (owner == currentThread) {
                locked(entityId);
                return;
            }
            try {
                while (true){
                    checkDeadLock(owner, entityId);
                    wait();
                    if (owner == null) {
                        locked(entityId);
                        return;
                    }
                }
            } finally {
                thread2waiting.remove(currentThread);
            }
        }

        private synchronized void locked(Object entityId) {
            owner = Thread.currentThread();
            id2thread.put(entityId, owner);
            lockedByThisThread().add(entityId);
        }

        private synchronized void unlock(Object entityId) {
            Thread currentThread = Thread.currentThread();
            if (owner != currentThread) {
                throw new IllegalStateException(currentThread + " is not owner of Lock for " + entityId);
            }
            Deque lockedByThread = lockedByThisThread();
            lockedByThread.removeLastOccurrence(entityId);
            if (!lockedByThread.contains(entityId)) {
                owner = null;
                id2thread.remove(entityId);
                notifyAll();
            }
        }

        Thread testOnly_owner() {
            return owner;
        }
    }

    /**
     * Exception to signal that locking is impossible without DeadLock
     * unless the thread releases 'entityID' lock.
     */
    public static class DeadLockException extends Exception {
        public final Object entityID;
        public DeadLockException(Object entityID) {
            this.entityID = entityID;
        }
    }
    /**
     * package private method to be used only by tests to verify the guts.
     */
    synchronized EntityLock testOnly_getLock(Object id) {
        return id2lock.get(id);
    }

}
