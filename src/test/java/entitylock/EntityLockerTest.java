/*
 * Copyright (c) 2017, Dmitry Fazunenko and/or his affiliates.
 * All rights reserved.
 */
package entitylock;

import org.testng.annotations.Test;

import java.util.*;
import java.util.concurrent.*;
import static org.testng.Assert.*;

/**
 * Test verifying various aspects of EntityLocker behavior.
 */
public class EntityLockerTest {

    @Test
    void testGetEntityLocker() {
        // assertion EntityLocker.getEntityLocker(object) returns the same object only for equal objects
        EntityLocker locker1 = EntityLocker.getEntityLocker(new Long(100));
        EntityLocker locker2 = EntityLocker.getEntityLocker(new Integer(100));
        EntityLocker locker3 = EntityLocker.getEntityLocker(new Long(100));
        assertTrue(locker1 == locker3);
        assertFalse(locker1 == locker2);
    }

    @Test
    void testReentrantSingleThread() {
        EntityLocker locker = EntityLocker.getEntityLocker(new Object());
        MultiLocker m = new MultiLocker(locker, "a", "b", "a");
        m.run();
        assertTrue(m.finished);
    }

    @Test
    void testParallel() {
        // no dead lock is expected
        EntityLocker locker = EntityLocker.getEntityLocker(new Object());
        MultiLocker m1 = new MultiLocker(locker, "a", "b", "c", "d");
        MultiLocker m2 = new MultiLocker(locker,  "a", "a", "a", "b", "b", "a");
        MultiLocker m3 = new MultiLocker(locker,  "a", "b", "b", "a");
        execute(m1, m2, m3);
        assertTrue(m1.finished);
        assertTrue(m2.finished);
        assertTrue(m3.finished);
    }

    @Test
    void testWaiting() throws InterruptedException, EntityLocker.DeadLockException {
        // assertion under test:
        // If there's a concurrent request to lock the same entity,
        // the other thread should wait until the entity becomes available.
        EntityLocker locker = EntityLocker.getEntityLocker("ABC");
        locker.lock("a");
        MultiLocker m = new MultiLocker(locker, "a");
        Thread t = new Thread(m);
        t.start();
        assertFalse(m.finished);
        Thread.sleep(100); // bad practice, but ok in case
        assertEquals(t.getState(), Thread.State.WAITING);
        locker.unlockAll();
        Thread.sleep(100); // bad practice, but ok in case
        assertTrue(m.finished);
        assertEquals(t.getState(), Thread.State.TERMINATED);
    }

    @Test
    void kitchensinkTest() {
        // run many threads with random IDs
        // expected: no hangs

        final int N = 10;   // max resources
        final int T = 100;  // threads
        MultiLocker[] ml = new MultiLocker[T];
        Random random = new Random(2128506);
        EntityLocker locker = EntityLocker.getEntityLocker(new Object());
        for (int i = 0; i < T; i++) {
            Integer[] ids = new Integer[random.nextInt(T)];
            for (int j = 0; j < ids.length; j++) {
                ids[j] = random.nextInt(N);
            }
            ml[i] = new MultiLocker(locker, ids);
        }
        execute(ml);
        int deadLockCount = 0;
        for (int i = 0; i < ml.length; i++) {
            if (!ml[i].finished) {
                deadLockCount++;
            }
        }
        assertTrue(deadLockCount < T); // at least one thread must be successful
    }

    @Test
    void testLockUnlock() throws InterruptedException, EntityLocker.DeadLockException {
        // assertion under: same lock objects for equal IDs
        EntityLocker locker = EntityLocker.getEntityLocker(new Object());
        locker.lock(new Integer(10));
        locker.lock(new Integer(30));
        EntityLocker.EntityLock lock1 = locker.testOnly_getLock(new Integer(10));
        EntityLocker.EntityLock lock2 = locker.testOnly_getLock(new Integer(10));
        EntityLocker.EntityLock lock3 = locker.testOnly_getLock(new Integer(30));

        assertEquals(lock1.testOnly_owner(), Thread.currentThread());
        assertEquals(lock3.testOnly_owner(), Thread.currentThread());
        locker.unlock(new Integer(10));
        assertNull(lock1.testOnly_owner());
        assertEquals(lock3.testOnly_owner(), Thread.currentThread());

        assertNotNull(lock1);
        assertTrue(lock1 == lock2); // locks for equal IDs is the same object
        assertNotEquals(lock1, lock3);
    }

    @Test(expectedExceptions = IllegalStateException.class)
    void testUnlockNegative() {
        EntityLocker.getEntityLocker(new Object()).unlock(new Integer(22));
    }

    @Test
    void testUnlockAll() throws InterruptedException {
        EntityLocker locker = EntityLocker.getEntityLocker(new Object());
        locker.unlockAll(); // no exception is expected
        locker.tryLock("a");
        locker.tryLock("b");
        locker.unlockAll();
        MultiLocker m = new MultiLocker(locker, "a", "b");
        m.run();
        assertTrue(m.finished);
    }

    @Test
    void testDeadLock() throws InterruptedException {
        EntityLocker locker = EntityLocker.getEntityLocker(new Object());
        CountDownLatch latch = new CountDownLatch(2);
        Object a = "Object_A";
        Object b = "Object_B";
        DeadLocker d1 = new DeadLocker(locker, a, b, latch);
        DeadLocker d2 = new DeadLocker(locker, b, a, latch);
        execute(d1, d2);
        assertTrue(d1.deadLocked == null && d2.deadLocked == b || d2.deadLocked == null && d1.deadLocked == a);
    }

    @Test
    void testNoDeadLockOnDifferentResource() throws InterruptedException {
        // no dead lock on different resources
        CountDownLatch latch = new CountDownLatch(2);
        Object a = "Object_A";
        Object b = "Object_B";
        DeadLocker d1 = new DeadLocker(EntityLocker.getEntityLocker(new Object()), a, b, latch);
        DeadLocker d2 = new DeadLocker(EntityLocker.getEntityLocker(new Object()), b, a, latch);
        execute(d1, d2);
        assertNull(d1.deadLocked);
        assertNull(d2.deadLocked);
    }

    @Test
    void testDeadLockN() throws InterruptedException {
        EntityLocker locker = EntityLocker.getEntityLocker(new Object());
        Object[] objects = {"Object_A", "Object_B", "Object_C", "Object_D", "Object_E"};
        final int N = objects.length;
        DeadLocker[] deadLockers = new DeadLocker[N];
        CountDownLatch latch = new CountDownLatch(N);
        for (int i = 0; i < N; i++) {
            deadLockers[i] = new DeadLocker(locker, objects[i], objects[(i+1)%N], latch);
        }
        execute(deadLockers);
        int deadLockCount = 0;
        for (int i = 0; i < N; i++) {
            if (deadLockers[i].deadLocked != null) {
                deadLockCount++;
                assertEquals(deadLockers[i].deadLocked, objects[i]);
            }
        }
        assertEquals(deadLockCount, 1);
    }

    @Test
    void testWeakHashMap() throws EntityLocker.DeadLockException, InterruptedException {
        EntityLocker locker = EntityLocker.getEntityLocker(new Object());
        Integer existing = new Integer(12345);
        Integer deleted = new Integer(-100);
        locker.lock(existing);
        locker.lock(deleted);
        locker.unlock(deleted);
        deleted = null;
        System.gc();
        assertNull(locker.testOnly_getLock(new Integer(-100)));
        assertNotNull(locker.testOnly_getLock(existing));
    }

    @Test
    void testEntityLockerWeakHashMap() {
        // check: HashMap to store EntityLocker instances is cleaning when resource instances become unreachable.
        Object resource1 = new Long(100);
        Object resource2 = new Long(200);
        int locker1code = System.identityHashCode((EntityLocker.getEntityLocker(resource1)));
        int locker2code = System.identityHashCode((EntityLocker.getEntityLocker(resource2)));
        resource2 = null;
        System.gc();
        int locker1_2code = System.identityHashCode((EntityLocker.getEntityLocker(new Long(100))));
        int locker2_2code = System.identityHashCode((EntityLocker.getEntityLocker(new Long(200))));
        assertEquals(locker1code, locker1_2code);
        assertNotEquals(locker2code, locker2_2code);
    }

    // utility method to execute multiple tasks in parallel and join.
    static void execute(Runnable... tasks) {
        ExecutorService executor = Executors.newFixedThreadPool(tasks.length);

        CompletableFuture<?>[] futures = Arrays.asList(tasks).stream()
                .map(task -> CompletableFuture.runAsync(task, executor))
                .toArray(CompletableFuture[]::new);
        CompletableFuture.allOf(futures).join();
        executor.shutdown();
    }


    /**
     * Class to allocate given ids, one by one.
     */
    static class MultiLocker implements Runnable {
        final List ids;
        final EntityLocker locker;
        volatile boolean finished = false;
        volatile Exception thrown = null;


        MultiLocker(EntityLocker locker, Object... ids) {
            this.locker = locker;
            this.ids = Arrays.asList(ids);
        }

        public void run() {
            try {
                for (Object entityId: ids)  {
                    locker.lock(entityId);
                }
                finished = true;
            } catch (Exception e) {
                thrown = e;
            } finally {
                locker.unlockAll();
            }

        }
    }

    /**
     * Class to cause deadlock
     */
    static class DeadLocker implements Runnable {
        final EntityLocker locker;
        final CountDownLatch latch;
        final Object a,b;
        volatile Object deadLocked = null;


        DeadLocker(EntityLocker locker, Object a, Object b, CountDownLatch latch) {
            this.locker = locker;
            this.latch = latch;
            this.a = a;
            this.b = b;
        }

        public void run() {
            try {
                locker.tryLock(a);
                latch.countDown();
                latch.await();
                deadLocked = locker.tryLock(b);
                if (deadLocked != null) {
                    locker.unlock(deadLocked);
                } else {
                    locker.unlock(b);
                    locker.unlock(a);
                }
                return;
            } catch (InterruptedException e) {
                // should never happen
                throw new Error(e);
            }
        }
    }

}